module BSSMF
using MKL
# using MKLSparse
# Utils
include("common/lpProjs.jl")
include("common/nnls_fpgm.jl")
include("common/powermethod.jl")
include("common/snpa.jl")
include("common/utils.jl")

# Methods
include("methods/bssmf.jl")
# include("methods/obssmf.jl")
end # module