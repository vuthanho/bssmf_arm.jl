using LinearAlgebra
using Random
using SparseArrays
using Statistics
using DelimitedFiles

# Custom packages
using BSSMF
using PlotlyLight
using MAT

println("number of threads : $(Threads.nthreads())")

dataset = "ml-1m"
path_tr = pwd()*"/xp/"*dataset*"/data_tr.mat"
path_te = pwd()*"/xp/"*dataset*"/data_te.mat"
r=5

dummy = Workspace(path_tr,r)
bssmf!(dummy,centering=false,maxiter=2,inneriter=1,verbose=false,extra=true)

maxiter = 200
inneriter=1

# Beginning of the tests

function sparse_run(maxiter,inneriter,path_tr,path_te,r,extra,centering,seed)
    Random.seed!(seed)
    wrkspc_tr = Workspace(path_tr,r)
    wrkspc_te = Workspace(path_te,r)
    if (centering isa Bool) && centering
        meanX = mean(wrkspc_tr.V)
        wrkspc_tr.V .-= meanX
        wrkspc_te.V .-= meanX
        wrkspc_te.Vtest .-= meanX
    end
    if !(centering isa Bool)
        wrkspc_tr.V .+= centering
        wrkspc_te.V .+= centering
        wrkspc_te.Vtest .+= centering
    end
    err,_ = bssmf!(wrkspc_tr,maxiter=maxiter,inneriter=inneriter,verbose=false,extra=extra)
    wrkspc_te.W = wrkspc_tr.W
    rmse_te = pred!(wrkspc_te)
    return err,rmse_te
end

function sparse_runs(runs,maxiter,inneriter,path_tr,path_te,r)
    errs = zeros(maxiter,6)
    rmses_te = zeros(6)
    for run in 1:runs
        print("run $run/$runs")
        # no extra no centering
        err,rmse_te = sparse_run(maxiter,inneriter,path_tr,path_te,r,false,false,run)
        errs[:,1] .+= err
        rmses_te[1] += rmse_te
        print(".")
        # extra no centering
        err,rmse_te = sparse_run(maxiter,inneriter,path_tr,path_te,r,true,false,run)
        errs[:,2] .+= err
        rmses_te[2] += rmse_te
        print(".")
        # no extra centering
        err,rmse_te = sparse_run(maxiter,inneriter,path_tr,path_te,r,false,true,run)
        errs[:,3] .+= err
        rmses_te[3] += rmse_te
        print(".")
        # extra centering
        err,rmse_te = sparse_run(maxiter,inneriter,path_tr,path_te,r,true,true,run)
        errs[:,4] .+= err
        rmses_te[4] += rmse_te
        print(".")
        # no extra bad centering
        err,rmse_te = sparse_run(maxiter,inneriter,path_tr,path_te,r,false,10,run)
        errs[:,5] .+= err
        rmses_te[5] += rmse_te
        print(".")
        # extra bad centering
        err,rmse_te = sparse_run(maxiter,inneriter,path_tr,path_te,r,true,10,run)
        errs[:,6] .+= err
        rmses_te[6] += rmse_te
        print("\r                      \r")
    end
    return errs./runs,rmses_te./runs
end

errs,rmses_te = sparse_runs(10, maxiter, inneriter, path_tr, path_te, r)

display(rmses_te)

trace1 = Config(y=errs[:,1],name="plain")
trace2 = Config(y=errs[:,2],name="TITAN")
trace3 = Config(y=errs[:,3],name="centering")
trace4 = Config(y=errs[:,4],name="TITAN+centering")
trace5 = Config(y=errs[:,5],name="bad center")
trace6 = Config(y=errs[:,6],name="TITAN+bad center")
plot_ml1m = Plot([trace1,trace2,trace3,trace4,trace5,trace6],Config(showlegend=true))

writedlm("xp/ml1m_extra_center.txt",hcat(1:maxiter,errs))

# ############### MNIST ##################
path = pwd()*"/xp/mnist.mat"
r=50
X = matread(path)["X"]
dummy = Workspace(X,r)
bssmf!(dummy,maxiter=2,inneriter=1,verbose=false,extra=true)

maxiter = 25
inneriter=10

# Beginning of the tests

function dense_run(maxiter,inneriter,path,r,extra,centering,seed)
    Random.seed!(seed)
    X = matread(path)["X"]
    wrkspc = Workspace(X,r)
    if (centering isa Bool) && centering
        meanX = mean(wrkspc.X)
        wrkspc.X .-= meanX
    end
    if !(centering isa Bool)
        wrkspc.X .+= centering
    end
    err,_ = bssmf!(wrkspc,maxiter=maxiter,inneriter=inneriter,verbose=false,extra=extra)
    return err
end

function dense_runs(runs,maxiter,inneriter,path,r)
    errs = zeros(maxiter,6)
    for run in 1:runs
        print("run $run/$runs")
        # no extra no centering
        err = dense_run(maxiter,inneriter,path,r,false,false,run)
        errs[:,1] .+= err
        print(".")
        # extra no centering
        err = dense_run(maxiter,inneriter,path,r,true,false,run)
        errs[:,2] .+= err
        print(".")
        # no extra centering
        err = dense_run(maxiter,inneriter,path,r,false,true,run)
        errs[:,3] .+= err
        print(".")
        # extra centering
        err = dense_run(maxiter,inneriter,path,r,true,true,run)
        errs[:,4] .+= err
        print(".")
        # no extra bad centering
        err = dense_run(maxiter,inneriter,path,r,false,1,run)
        errs[:,5] .+= err
        print(".")
        # extra bad centering
        err = dense_run(maxiter,inneriter,path,r,true,1,run)
        errs[:,6] .+= err
        print("\r                      \r")
    end
    return errs./runs
end

errs = dense_runs(10, maxiter, inneriter, path, r)
trace1 = Config(y=errs[:,1],name="plain")
trace2 = Config(y=errs[:,2],name="TITAN")
trace3 = Config(y=errs[:,3],name="centering")
trace4 = Config(y=errs[:,4],name="TITAN+centering")
trace5 = Config(y=errs[:,5],name="bad center")
trace6 = Config(y=errs[:,6],name="TITAN+bad center")
plot_mnist = Plot([trace1,trace2,trace3,trace4,trace5,trace6],Config(showlegend=true))

writedlm("xp/mnist_extra_center.txt",hcat(1:maxiter,errs))