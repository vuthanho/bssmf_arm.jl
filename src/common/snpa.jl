using BSSMF
using LinearAlgebra
using SparseArrays

function snpa(X::AbstractMatrix{T},
    r::Integer;
    options::Dict{String,Number}=Dict{String,Number}()) where T <: AbstractFloat

    m, n = size(X)
    default_options_snpa!(options)
    showiter = options["showiter"]
    relerr = options["relerr"]
    normX0 = sum(X.^2, dims=1)
    nXmax = maximum(normX0)
    normR = normX0
    XtUK = T(NaN) .* Matrix{T}(undef, n, r)
    UKtUK = T(NaN) .* Matrix{T}(undef, r, r)
    K = Vector{Int}(undef, r)
    U = T(NaN) .* Matrix{T}(undef, m, r)
    H = T(NaN) .* Matrix{T}(undef, r, n)

    #  Performs r recursion steps (unless the relative approximation error is 
    #  smaller than relerr)
    if showiter == 1
        println("Extraction of the indices by SNPA")
    end
    i = 1
    # Main loop
    while i <= r && sqrt(maximum(normR) / nXmax) > relerr
        if showiter == 1
            print("$i ... ")
        end
        # select the column of the residual R with largest l2-norm
        a = maximum(normR)
        # Check ties up to 1e-6 precision
        b = findall((a .- normR) / a .<= 1e-6)
        # In case of a tie, select column with largest norm of the input matrix
        _, d = findmax(normX0[b])
        b = b[d]
        # Update the index set, and extracted column
        K[i] = b
        U[:,i] = X[:,b]
        # Update XtUK
        XtUK[:,i] = X'U[:,i]
        # Update UKtUK
        if i == 1
            UKtUK[i,i] = U[:,i]'U[:,i]
        else
            UtUi = U[:,1:i - 1]'U[:,i]
            UKtUK[1:i,1:i] = [[UKtUK[1:i-1,1:i-1] UtUi];[UtUi' U[:,i]'U[:,i]]]
        end
        # Update residual
        if i == 1
            H[1,:] = nnls_fpgm(X, X[:,K[1]], options=options)
        else
            H[:,K[i]] .= zero(T)
            h = zeros(T, n)
            h[K[i]] = one(T)
            H[i,:] = h'
            H[1:i,:] = nnls_fpgm(X,X[:,K[1:i]],init=H[1:i,:],options=options)
        end
        # Update the norm of the columns of the residual without computing
        # it explicitely
        if i==1
            normR = normX0 .- 2*(XtUK[:,1]' .* H[1,:]') + (H[1,:]' .* (UKtUK[1,1]*H[1,:]'))
        else
            normR = normX0 .- 2*sum(XtUK[:,1:i]' .* H[1:i,:],dims=1) + sum(H[1:i,:] .* (UKtUK[1:i,1:i]*H[1:i,:]),dims=1)
        end
        i+=1
    end
    if showiter==1
        println()
    end
    return K,H
end

function default_options_snpa!(options::Dict{String,Number})
    if !haskey(options, "scaling")
        options["scaling"] = 1
    end
    if !haskey(options, "showiter")
        options["showiter"] = 0
    end
    if !haskey(options, "maxiter")
        options["maxiter"] = 200
    end
    if !haskey(options, "relerr")
        options["relerr"] = 1e-6
    end
    if !haskey(options, "proj")
        options["proj"] = 0
    end
end